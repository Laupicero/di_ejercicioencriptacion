﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;

namespace ConexionEncriptada.CONEXION
{
    public partial class ConexionManual : Form
    {
        private LIBRERIA.AES aes = new LIBRERIA.AES();
        string dbcnString;
        private SqlConnection conexion;


        // Constructor
        public ConexionManual()
        {
            InitializeComponent();
        }


        // LOAD
        private void ConexionManual_Load(object sender, EventArgs e)
        {
            ReadfromXML();
            ConnectToDB();
        }

        


        //--------------------------------
        // EVENTOS BOTONES / ELEMENTOS
        //--------------------------------


        //Botón Generar Cadena de Conexión
        // Y nos la guardara en nuestro XML encriptado
        private void btnGuadar_Click(object sender, EventArgs e)
        {
            SavetoXML(this.aes.Encrypt(txtCnString.Text, LIBRERIA.Desencryptacion.appPwdUnique, int.Parse("256")));
        }


        

        // Evento Key-Press 'txtNombre' para realizar la consulta
        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ConsultDB();
            }
        }


        // Evento Key-Press 'txtApellido' para poder insertar el apellido en la DB 
        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtApellido.TextLength >= 0)
                {
                    InsertUpdateAPPATERNO();
                }
                else
                    MessageBox.Show("Inserte un apellido", "ERROR");
            }                
        }

       




        //--------------------------------
        //  MÉTODOS AUXILIARES
        //--------------------------------

        //Nos lee la cadena de conexión encriptada, desencriptándola con la librería
        public void ReadfromXML()
        {
            try
            {

                XmlDocument doc = new XmlDocument();
                doc.Load("ConnectionString.xml");
                XmlElement root = doc.DocumentElement;
                dbcnString = root.Attributes[0].Value;
                txtCnString.Text = this.aes.Decrypt(dbcnString, LIBRERIA.Desencryptacion.appPwdUnique, int.Parse("256"));
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {

            }
        }

        // Nos establece la Conexion
        private void ConnectToDB()
        {
            try
            {
                //Establecemos la conexión
                this.conexion = new SqlConnection(txtCnString.Text);
                conexion.Open();
                MessageBox.Show("Conexion exitosa a SQL Server");
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Problema al tratar de conectar a BD. Detalles: \n" + ex.Message.ToString());
            }            
        }


        // Cargar del XML
        public void SavetoXML(Object dbcnString)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("ConnectionString.xml");
            XmlElement root = doc.DocumentElement;
            root.Attributes[0].Value = Convert.ToString(dbcnString);
            XmlTextWriter writer = new XmlTextWriter("ConnectionString.xml", null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
            writer.Close();
        }


        //--------------------------------
        // QUERYS / CONSULTAS
        //--------------------------------

        // Nos realiza una consulta a nuestra DB
        // A través del nombre del Empleado, nos devolverá el apellido paterno




        // Realiza la consulta y nos rellena el 'txtApellido'
        // A partir del nombre del Empleado
        private void ConsultDB()
        {
            SqlCommand cmd = new SqlCommand("Select APPATERNO FROM Empleado WHERE NOMBRE = '" +txtNombre.Text+ "'", this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            //Rellenamos el listado de Empleados
            while (rd.Read())
            {
                try
                {
                    txtApellido.Text = this.aes.Decrypt(rd["APPATERNO"].ToString(), LIBRERIA.Desencryptacion.appPwdUnique, int.Parse("256"));
                }
                catch (Exception e)
                {
                    MessageBox.Show("Excepción: " + e.Message, "ERROR");
                    rd.Close();
                }
            }
            rd.Close();
        }


        /// <summary>
        ///  Nos inserta el apellido del Empleado, pero Encriptado 
        /// </summary>
        private void InsertUpdateAPPATERNO()
        {
            try
            {
                String apellido = this.aes.Encrypt(txtApellido.Text, LIBRERIA.Desencryptacion.appPwdUnique, int.Parse("256"));
                String query = "UPDATE Empleado SET APPATERNO = @apellido1 Where NOMBRE = @nombre";

                SqlCommand command = new SqlCommand(query, this.conexion);

                command.Parameters.AddWithValue("@nombre", txtNombre.Text);
                command.Parameters.AddWithValue("@apellido1", apellido);

                command.ExecuteNonQuery();

                MessageBox.Show("¡Empleado Actualizado con ÉXITO!", "OPERACIÓN REALIZADA");
            }
            catch (Exception e)
            {
                MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
            }
        }


    }
}
